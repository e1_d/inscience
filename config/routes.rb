Rails.application.routes.draw do

  devise_for :users, :controllers => {:registrations => "registrations" }

  root 'welcome#show'


  resources :users

  resources :events do
    resources :posts, shallow: true do
      resources :comments, shallow: true
    end
    resources :applications
  end

  post 'events/:id/participants/new' => 'events#add_participant', as: :participant
  delete 'events/:id/participants/:participant_id' => 'events#delete_participant', as: :delete_participant
  get 'events/:id/participants' => 'events#show_participants', as: :participants
  get 'users/:id/applications' => 'users#show_applications', as: :user_applications

end
