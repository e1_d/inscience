class AddEventReferenceToApplications < ActiveRecord::Migration
  def change
    add_reference :applications, :event, foreign_key: true, index: true
  end
end
