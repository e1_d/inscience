class PostModelFields < ActiveRecord::Migration
  def change
    add_column :posts, :text, :text
    add_column :posts, :date, :datetime
  end
end
