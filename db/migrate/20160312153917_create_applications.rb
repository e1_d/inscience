class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string :phone
      t.string :name
      t.text :description
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
