class ChangeInfoType < ActiveRecord::Migration
  def change
    change_column :users, :info, :text
  end
end
