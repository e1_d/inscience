class UserModelFields < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string, null: false
    add_column :users, :last_name, :string, null: false
    add_column :users, :patronymic, :string
    add_column :users, :date_of_birth, :date, null: false
    add_column :users, :city, :string, null: false
    add_column :users, :info, :string
  end
end
