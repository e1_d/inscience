class EventModelFields < ActiveRecord::Migration
  def change
    add_column :events, :name, :string, null: false
    add_column :events, :date, :date
    add_column :events, :begin_time, :time
    add_column :events, :end_time, :time
    add_column :events, :city, :string, null: false
    add_column :events, :address, :string
    add_column :events, :description, :text
  end
end
