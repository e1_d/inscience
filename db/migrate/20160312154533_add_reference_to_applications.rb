class AddReferenceToApplications < ActiveRecord::Migration
  def change
    add_reference :applications, :status, index: true, foreign_key: true
  end
end
