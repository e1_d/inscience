class Event < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: "user_id"
  has_many :participants
  has_many :users, through: :participants
  has_many :posts
  has_many :applications
end
