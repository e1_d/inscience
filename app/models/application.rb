class Application < ActiveRecord::Base
  belongs_to :user
  belongs_to :event
  has_one :status
end
