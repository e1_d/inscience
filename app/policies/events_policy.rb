class EventsPolicy

  def initialize(user, event)
    @user = user
    @event = event
  end

  def access?
    @user == @event.creator
  end

  def participant?
    @user.events.include? @event
  end

end