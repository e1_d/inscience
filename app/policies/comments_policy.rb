class CommentsPolicy

  def initialize(user, comment)
    @user = user
    @comment = comment
  end

  def author?
    @user == @comment.user
  end

end