class EventsController < ApplicationController

  before_action :define_event, except: [:new, :create, :index]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :is_author, only: [:edit, :update,:delete, :destroy]

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    @event.creator = current_user

    if @event.save
      redirect_to @event, notice: "Конференция успешно создана"
    else
      redirect_to "/", notice: "При создании конференции возникли проблемы"
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @events = Event.all
  end

  def show
    @user = @event.creator
    @posts = @event.posts.all
    @post = @event.posts.build
  end

  def delete
  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_path, notice: 'Конференция была успешно удалена' }
      format.json { head :no_content }
    end
  end

  def add_participant
    @participant = Participant.new
    @participant.user = current_user
    @participant.event = @event
    @participant.save
    redirect_to @event
  end

  def show_participants
    if @event.creator != current_user
      redirect_to @event
    end
    @participants = @event.participants
  end

  def delete_participant
    @user = User.find(params[:participant_id])
    @participant = Participant.find_by(user_id: @user.id, event_id: params[:id])
    @participant.destroy
    redirect_to @event
  end

  private
  def define_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:name, :date, :begin_time, :end_time,
                                  :city, :address, :description)
  end

  def is_author
    if @event.creator.id != current_user.id
      redirect_to @event
    end
  end

end
