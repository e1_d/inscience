class ApplicationsController < ApplicationController

  before_action :define_event, only: [:edit,  :update, :new, :create]
  before_action :define_application, only: [:edit, :update, :delete, :destroy, :show, :is_author]
  before_action :authenticate_user!, only: [:new, :create, :destroy, :is_author]
  before_action :is_author, only: [:edit, :update, :delete, :destroy]

  def show
  end

  def index
    @applications = @event.applications
  end

  def new
    @application = Application.new
  end

  def create
    @application = @event.applications.build(application_params)
    @application.user = current_user
    @application.save
    if @application.save
      redirect_to @event
    else
      redirect_to new_event_application_path
    end
  end

  def edit
  end

  def update
    if @application.update(application_params)
      redirect_to user_applications_path(current_user)
    else
      redirect_to edit_event_application_path
    end
  end

  def delete

  end

  def destroy
    @application.destroy
    redirect_to user_applications_path(current_user)
  end

  private

  def application_params
    params.require(:application).permit(:phone, :name, :description)
  end

  def define_event
    @event = Event.find(params[:event_id])
  end

  def define_application
    @application = Application.find(params[:id])
  end

  def is_author
    if @application.user != current_user
      redirect_to @application.event
    end
  end

end
