class RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name,
                                 :last_name, :patronymics, :date_of_birth, :city, :info)
  end

  def account_update_params
    params.require(:user).permit(:password, :password_confirmation, :first_name,
                                 :last_name, :patronymics, :date_of_birth, :city, :info)
  end
end
