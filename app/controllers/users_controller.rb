class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :define_user
  def show
  end

  def show_applications
    @applications = @user.applications
  end

  def define_user
    @user = User.find(params[:id])
  end

end
