class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :destroy]
  before_action :define_post, only: [:destroy, :show]

  def create
    @event = Event.find(params[:event_id])
    @post = @event.posts.build(post_params)
    @post.user = current_user
    respond_to do |format|
      if @post.save
        format.html { redirect_to @event, notice: 'Post was successfully created.' }
        format.js {  }
      else
        format.html { render :new }
      end
    end
  end

  def show
    @comment = @post.comments.build
    @comments = @post.comments.all
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to @post.event, notice: 'Post was successfully destroyed.' }
      format.js { }
    end
  end

  private

  def define_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:text)
  end

end
